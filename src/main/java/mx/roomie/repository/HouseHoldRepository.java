package mx.roomie.repository;

import org.springframework.data.repository.CrudRepository;

import mx.roomie.model.HouseHold;

public interface HouseHoldRepository extends CrudRepository<HouseHold, Integer> {

}