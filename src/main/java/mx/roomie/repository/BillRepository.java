package mx.roomie.repository;

import org.springframework.data.repository.CrudRepository;

import mx.roomie.model.Bill;

public interface BillRepository extends CrudRepository<Bill, Integer> {}