package mx.roomie.repository;

import org.springframework.data.repository.CrudRepository;

import mx.roomie.model.Task;

public interface TaskRepository extends CrudRepository<Task, Integer> {

}