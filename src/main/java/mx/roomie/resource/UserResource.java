package mx.roomie.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import mx.roomie.model.Token;
import mx.roomie.model.User;
import mx.roomie.request.UserRequest;
import mx.roomie.service.UserService;

@RestController
public class UserResource {

  @Autowired
  private UserService userService;

  @PostMapping("/signup")
  public ResponseEntity<User> signup(@RequestBody UserRequest request) {
    User user = userService.saveUser(request);

    return ResponseEntity.status(HttpStatus.CREATED).body(user);
  }

  @PostMapping("/login")
  public ResponseEntity<Token> login(@RequestBody UserRequest request) {
    User user = userService.getUserByEmail(request.getEmail());

    if (user == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    if ( !(new BCryptPasswordEncoder().matches(request.getPassword(), user.getPassword())) ) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    String token = userService.generateToken(request.getEmail());
    Token tokenResponse = new Token();
    tokenResponse.setToken(token);

    return ResponseEntity.ok().body(tokenResponse);
  }

  @GetMapping("/users")
  public ResponseEntity<List<User>> getUsers() {
    List<User> users = userService.getUsers();
    ResponseEntity<List<User>> response = ResponseEntity.ok().body(users);
    return response;
  }

  @GetMapping("/user/{id}")
  public ResponseEntity<User> getUser(@PathVariable Integer id) {
    Optional<User> userOptional = userService.getUserById(id);

    if (!userOptional.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    
    User user = userOptional.get();
    return ResponseEntity.ok().body(user);
    
  }

  @DeleteMapping("/user/{id}")
  public ResponseEntity<User> deleteUser(@PathVariable Integer id) {
    userService.deleteUser(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  @PutMapping("/user/{id}")
  public ResponseEntity<User> updateUser(@PathVariable Integer id, @RequestBody UserRequest request) {
    Optional<User> userOptional = userService.getUserById(id);

    if (!userOptional.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    User user = userService.saveUser(request, id, userOptional);

    return ResponseEntity.ok().body(user);
  }

}