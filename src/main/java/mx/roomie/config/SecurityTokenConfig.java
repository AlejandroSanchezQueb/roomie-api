package mx.roomie.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import mx.roomie.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserService userService;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .csrf()
      .disable()
      .httpBasic()
      .disable()
      .authorizeRequests()
      .antMatchers("/", "/login", "/signup")
      .permitAll()
      .anyRequest()
      .authenticated()
      .and()
      .addFilterBefore(new JwtFilter(userService), UsernamePasswordAuthenticationFilter.class);
  }
}