package mx.roomie.config;

import java.security.Key;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class SecurityConstants {
  public static final Key SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS512);
}