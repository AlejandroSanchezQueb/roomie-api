package mx.roomie.service;

import mx.roomie.model.Rule;
import mx.roomie.repository.RuleRepository;
import mx.roomie.request.RuleRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class RuleService {
    @Autowired
    private RuleRepository ruleRepository;

    public List<Rule> getRules() {
        List<Rule> rules = new LinkedList<>();
        ruleRepository.findAll().iterator().forEachRemaining(rules::add);
        return rules;
    }

    public Optional<Rule> getRuleById(Integer id) {
        return ruleRepository.findById(id);
    }

    public Rule saveRule(RuleRequest request) {
        Rule rule = new Rule();
        rule.setTitle(request.getTitle() != null? request.getTitle() : "");
        rule.setDescription(request.getDescription() != null? request.getDescription() : "");
        rule.setHouseholdId(request.getHouseholdId() != null? request.getHouseholdId() : 0);
        
        ruleRepository.save(rule);

        return rule;
    }

    public Rule saveRule(RuleRequest request, @Nullable Integer id, @Nullable Optional<Rule> ruleOptional) {
        Rule rule = new Rule();

        if(id != null){
            rule.setId(id);
        }
        rule.setTitle(request.getTitle() != null? request.getTitle() : ruleOptional.get().getTitle());
        rule.setDescription(request.getDescription() != null? request.getDescription() : ruleOptional.get().getDescription());
        rule.setHouseholdId(request.getHouseholdId() != null? request.getHouseholdId() : ruleOptional.get().getHouseholdId());
        
        ruleRepository.save(rule);

        return rule;
    }

    public void deleteRule(Integer id) {
        ruleRepository.deleteById(id);
    }
}
